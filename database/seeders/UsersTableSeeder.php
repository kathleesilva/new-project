<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Imposto Adm',
            'email' => 'admin@impostonobolso.com',
            'password' => bcrypt('102102102'),
        ]);
    }
}
