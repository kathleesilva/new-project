@extends('adminlte::page')

@section('title', 'Agendamento')

@section('content_header')
    <h1>Cadastro de Agendamento</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('agendamentos.store') }}" method="POST">
                @csrf
                {{ method_field('POST') }}
                <div class="form-group col-md-5">
                    <label>Serviço</label>
                    <select class="form-control" name="service" required>
                        <option value="">Selecione um serviço</option>
                        @foreach (App\Models\Service::all() as $item)
                            <option value="{{ $item->name }}">{{ $item->name }}</option>
                        @endforeach
                    </select>
                    @error('service')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-5">
                    <label>Data/Horário</label>
                    <input type="datetime" name="dt_agendamento" class="form-control @error('dt_agendamento') is-invalid @enderror" required>
                    @error('dt_agendamento')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-5">
                    <label>Cliente</label>
                    <input type="text" name="cliente" class="form-control @error('cliente') is-invalid @enderror" required>
                    @error('cliente')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-5">
                    <label>Raça</label>
                    <input type="text" name="raca" class="form-control @error('raca') is-invalid @enderror" required>
                    @error('raca')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                   <button type="submit" class="btn btn-dark">Atualizar</button>
                </div>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>
@stop
