@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Agendamento do Cliente:<b>{{ $agendamento->cliente }}</b></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-body">
            <form action="{{ route('agendamentos.update', [$agendamento->id]) }}" method="POST">
                @csrf
                {{ method_field('PATCH') }}
                <div class="form-group col-md-8">
                    <label>Serviço</label>
                    <select name="company_id" id="" class="form-control">
                        @foreach (App\Models\Service::all() as $service)
                            <option value="{{ $service->id }}"
                                @if ($agendamento->service == $service->name) selected @endif
                                >{{ $service->name }}</option>
                        @endforeach
                    </select>
                    @error('service')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-5">
                    <label>Data/Horário</label>
                    <input type="datetime" name="dt_agendamento" class="form-control @error('dt_agendamento') is-invalid @enderror"
                        value="{{ $agendamento->dt_agendamento }}">
                    @error('dt_agendamento')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-5">
                    <label>Cliente</label>
                    <input type="text" name="cliente" class="form-control @error('cliente') is-invalid @enderror"
                        value="{{ $agendamento->cliente }}">
                    @error('cliente')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group col-md-5">
                    <label>Raça</label>
                    <input type="text" name="raca" class="form-control @error('raca') is-invalid @enderror"
                        value="{{ $agendamento->raca }}">
                    @error('raca')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-group">
                   <button type="submit" class="btn btn-dark">Atualizar</button>
                </div>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>
@stop
