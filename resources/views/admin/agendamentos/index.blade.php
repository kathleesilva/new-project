@extends('adminlte::page')

@section('title', 'Agendamentos')

@section('content_header')
      <h1>Agendamentos</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        Todos os agendamentos
                    </li>
                </ol>
            </nav>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5>
                        {{ Session::get('message') }}
                </div>
            @endif
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    @if (count($agendamentos) > 0)
                        <tr>
                            <th>Dt. Agendamento</th>
                            <th>Cliente</th>
                            <th>Raça</th>
                            <th style="width: 40px">Ações</th>
                        </tr>
                    @else
                        <td>Nenhum agendamento cadastrado</td>
                    @endif
                </thead>
                <tbody>
                    @foreach ($agendamentos as $agendamento)
                        <tr>
                            <td>{{ $agendamento->dt_agendamento }}</td>
                            <td>{{ $agendamento->cliente ?? 'S/N' }}</td>
                            <td>{{ $agendamento->raca ?? 'S/N' }}</td>
                            <td>
                                <div class="btn-agendamento btn-sm">
                                    <a href="{{ route('agendamentos.edit', [$agendamento->id]) }}" class="btn btn-warning"><i
                                        class="fas fa-edit"></i></a>

                                    <a href="#" class="btn btn-danger"
                                        data-toggle="modal" data-target="#modal-danger{{ $agendamento->id }}">
                                        <i class="fas fa-trash"></i></a>

                                    <div class="modal fade" id="modal-danger{{ $agendamento->id }}">
                                        <div class="modal-dialog">
                                            <form action="{{ route('agendamentos.destroy', [$agendamento->id]) }}" method="POST">
                                                @csrf
                                                {{ method_field('DELETE') }}
                                                <div class="modal-content bg-danger">
                                                    <div class="modal-header">
                                                        <h4 class="modal-title">Atenção</h4>
                                                        <button type="button" class="close" data-dismiss="modal"
                                                            aria-label="Close">
                                                            <span aria-hidden="true">&times;</span>
                                                        </button>
                                                    </div>
                                                    <div class="modal-body">
                                                        <p>Deseja realmente exluir  o agendamento do dia <b>{{ $agendamento->dt_agendamento }}</b> ?</p>
                                                    </div>
                                                    <div class="modal-footer justify-content-between">
                                                        <button type="submit" class="btn btn-outline-light"
                                                            data-dismiss="modal">Cancelar</button>
                                                        <button type="submit"
                                                            class="btn btn-outline-light">Excluir</button>
                                                    </div>
                                                </div>
                                            </form>
                                            <!-- /.modal-content -->
                                        </div>
                                        <!-- /.modal-dialog -->
                                    </div>
                                    @endif
                                    <!-- /.modal -->
                                </div>
                            </td>
                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer"></div>
    </div>
@stop
