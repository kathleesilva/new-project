@extends('adminlte::page')

@section('title', 'Serviço')

@section('content_header')
    <h1>Serviços</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
           
        </div>
        <div class="card-body">
            <form action="{{ route('services.store') }}" method="POST">
                @csrf
                {{ method_field('POST') }}
                <div class="form-service col-md-5">
                    <label>Tipo de Serviço</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                        value="{{ $service->name }}" required>
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-service col-md-5">
                    <label>Preço</label>
                    <input type="text" name="preco" class="form-control @error('preco') is-invalid @enderror"
                    placeholder="Descrição" value="{{ $service->preco }}" required>
                    @error('preco')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-service col-md-5">
                    <label>Data de Disponibilidade</label>
                    <input type="text" name="dt_disponivel" class="form-control @error('dt_disponivel') is-invalid @enderror"
                    placeholder="Descrição" value="{{ $service->dt_disponivel }}" required>
                    @error('dt_disponivel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-service">
                   <button type="submit" class="btn btn-dark">Atualizar</button>
                </div>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>
@stop
