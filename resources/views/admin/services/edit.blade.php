@extends('adminlte::page')

@section('title', 'Dashboard')

@section('content_header')
    <h1>Editar Serviço -  <b>{{ $service->name }}</b></h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
           
        </div>
        <div class="card-body">
            <form action="{{ route('services.update', [$service->id]) }}" method="POST">
                @csrf
                {{ method_field('PATCH') }}
                <div class="form-service col-md-5">
                    <label>Tipo de Serviço</label>
                    <input type="text" name="name" class="form-control @error('name') is-invalid @enderror"
                        value="{{ $service->name }}">
                    @error('name')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-service col-md-5">
                    <label>Preço</label>
                    <input type="text" name="preco" class="form-control @error('preco') is-invalid @enderror"
                    placeholder="Descrição" value="{{ $service->preco }}">
                    @error('preco')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-service col-md-5">
                    <label>Data de Disponibilidade</label>
                    <input type="text" name="dt_disponivel" class="form-control @error('dt_disponivel') is-invalid @enderror"
                    placeholder="Descrição" value="{{ $service->dt_disponivel }}">
                    @error('dt_disponivel')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                    @enderror
                </div>
                <div class="form-service">
                   <button type="submit" class="btn btn-dark">Atualizar</button>
                </div>
            </form>
        </div>
        <div class="card-footer"></div>
    </div>
@stop
