@extends('adminlte::page')

@section('title', 'Serviços')

@section('content_header')
      <h1>Serviços</h1>
@stop

@section('content')
    <div class="card">
        <div class="card-header">
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item active" aria-current="page">
                        Todos os serviços
                    </li>
                </ol>
            </nav>
            @if (Session::has('message'))
                <div class="alert alert-success alert-dismissible">
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                    <h5>
                        {{ Session::get('message') }}
                </div>
            @endif
        </div>
        <div class="card-body">
            <table class="table">
                <thead>
                    @if (count($services) > 0)
                        <tr>
                            <th>Nome</th>
                            <th>Preço</th>
                            <th>Data de disponibilidade</th>
                            <th style="width: 40px">Ações</th>
                        </tr>
                    @else
                        <td>Nenhum serviço cadastrado</td>
                    @endif
                </thead>
                <tbody>

                        @foreach ($services as $service)
                            <tr>
                                <td>{{ $service->name }}</td>
                                <td>{{ $service->preco }}</td>
                                <td>{{ $service->dt_disponivel ?? 'S/N' }}</td>
                                <td>
                                    <div class="btn-service btn-sm">

                                        <a href="{{ route('services.edit', [$service->id]) }}" class="btn btn-warning"><i
                                            class="fas fa-edit"></i></a>

                                        <a href="#" class="btn btn-danger"
                                            data-toggle="modal" data-target="#modal-danger{{ $service->id }}">
                                            <i class="fas fa-trash"></i></a>

                                        <div class="modal fade" id="modal-danger{{ $service->id }}">
                                            <div class="modal-dialog">
                                                <form action="{{ route('services.destroy', [$service->id]) }}" method="POST">
                                                    @csrf
                                                    {{ method_field('DELETE') }}

                                                    <div class="modal-content bg-danger">
                                                        <div class="modal-header">
                                                            <h4 class="modal-title">Atenção</h4>
                                                            <button type="button" class="close" data-dismiss="modal"
                                                                aria-label="Close">
                                                                <span aria-hidden="true">&times;</span>
                                                            </button>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>Deseja realmente exluir  o serviço <b>{{ $service->name }}</b> ?</p>
                                                        </div>
                                                        <div class="modal-footer justify-content-between">
                                                            <button type="submit" class="btn btn-outline-light"
                                                                data-dismiss="modal">Cancelar</button>
                                                            <button type="submit"
                                                                class="btn btn-outline-light">Excluir</button>
                                                        </div>
                                                    </div>
                                                </form>
                                                <!-- /.modal-content -->
                                            </div>
                                            <!-- /.modal-dialog -->
                                        </div>
                                        @endif
                                        <!-- /.modal -->

                                    </div>
                                </td>
                            </tr>
                        @endforeach
                </tbody>
            </table>
        </div>
        <div class="card-footer"></div>
    </div>
@stop
